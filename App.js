import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import HomePage from "./src/screens/HomePage";

const navigator = createStackNavigator(
  {
    Home: HomePage,
  },
  {
    initialRouteName: "Home",
  }
);

export default createAppContainer(navigator);
